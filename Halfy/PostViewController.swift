//
//  PostViewController.swift
//  Swiftagram
//
//  Created by Parth Saxena on 9/18/16.
//  Copyright © 2016 Socify. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class PostViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var contentTextView: UITextView!
    
    var imageFileName = ""
    var imageFileName1 = ""

    var pickerController: Int?
    
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var selectImageButton: UIButton!
    
    @IBOutlet weak var previewImageView1: UIImageView!
    @IBOutlet weak var selectImageButton1: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerController = 0
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func postTapped(_ sender: AnyObject) {
        
        if (self.imageFileName != "" && self.imageFileName1 !=  "") {
        // image has finished uploaded, save post
        if let uid = FIRAuth.auth()?.currentUser?.uid {
            
            FIRDatabase.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                if let userDictionary = snapshot.value as? [String: AnyObject] {
                    for user in userDictionary {
                        print(user)
                        if let username = user.value as? String {
                            if let title = self.titleTextField.text {
                                if let content = self.contentTextView.text {
                                    let postObject: Dictionary<String, Any> = [
                                        "uid" : uid,
                                        "title" : title,
                                        "content" : content,
                                        "username" : username,
                                        "image" : self.imageFileName,
                                        "imageLike": 0,
                                        "image1" : self.imageFileName1,
                                        "image1Like": 0
                                    ]
                                    FIRDatabase.database().reference().child("posts").childByAutoId().setValue(postObject)
                                    
                                    let alert = UIAlertController(title: "Success", message: "Your post has been sent!", preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                        // code will run when "OK" button is tapped
                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainVC")
                                        self.present(vc!, animated: true, completion: nil)
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                    print("Posted to Firebase.")
                                    
                                }
                            }
                        }
                    }
                }
            })
        }
            
        } else {
            // image has not finished uploading, give alert
            let alert = UIAlertController(title: "Please Wait", message: "Your image has not finished uploading, please wait...", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }

    @IBAction func selectImageTapped(_ sender: Any) {
        
        pickerController = 0
        let picker = UIImagePickerController()
        picker.delegate = self
        self.present(picker, animated: true, completion: nil)
    }
    @IBAction func selectImageTapped1(_ sender: Any) {
        
        pickerController = 1
        let picker = UIImagePickerController()
        picker.delegate = self
        self.present(picker, animated: true,completion: nil)
    }
    
    func uploadImage(image: UIImage) {
        let randomName = randomStringWithLength(length: 10)
        let imageData = UIImageJPEGRepresentation(image, 1.0)
        let uploadRef = FIRStorage.storage().reference().child("images/\(randomName).jpg")
        
        let uploadTask = uploadRef.put(imageData!, metadata: nil) { metadata, error in
            if error == nil {
                // success
                print("Successfully uploaded image.")
                if(self.pickerController == 0){
                    self.imageFileName = "\(randomName as String).jpg"}
                if(self.pickerController == 1){
                    self.imageFileName1 = "\(randomName as String).jpg"}
                
            } else {
                // error
                print("Error uploading image: \(error?.localizedDescription)")
            }
        }
    }
    
    func randomStringWithLength(length: Int) -> NSString {
        let characters: NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: NSMutableString = NSMutableString(capacity: length)
        
        for i in 0..<length {
            var len = UInt32(characters.length)
            var rand = arc4random_uniform(len)
            randomString.appendFormat("%C", characters.character(at: Int(rand)))
        }
        
        return randomString
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // will run if the user hits cancel
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        // will run the user finishes picking an image from photo library
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if(pickerController==0)
            {
            self.previewImageView.image = pickedImage
            self.selectImageButton.isEnabled = false
            self.selectImageButton.isHidden = true
            uploadImage(image: pickedImage)
            picker.dismiss(animated: true, completion: nil)
            }
            if(pickerController==1)
            {
            self.previewImageView1.image = pickedImage
            self.selectImageButton1.isEnabled = false
            self.selectImageButton1.isHidden = true
            uploadImage(image: pickedImage)
            picker.dismiss(animated: true, completion: nil)
            }
        }
    }
    

}
